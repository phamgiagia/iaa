from django.db import models


# Create your models here.
class Customer(models.Model):
    name = models.CharField(max_length=200)
    # dob = models.CharField(max_length=200)
    # national = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    # ward = models.CharField(max_length=200)
    # district = models.CharField(max_length=200)
    # city = models.CharField(max_length=200)
    # country = models.CharField(max_length=200)
    # sex = models.CharField(max_length=200)
    phone = models.CharField(max_length=200)
    mail = models.CharField(max_length=200)
    # eye_color = models.CharField(max_length=200)
    # hair_color = models.CharField(max_length=200)
    # height = models.CharField(max_length=200)
    license_time = models.CharField(max_length=200)
    # urgent_license = models.CharField(max_length=200)
    license_front = models.FileField(upload_to='documents/%Y/%m/%d/%H/%M/%S')
    license_back = models.FileField(upload_to='documents/%Y/%m/%d/%H/%M/%S')
    pic = models.FileField(upload_to='documents/%Y/%m/%d/%H/%M/%S')
    # identity_card = models.FileField(upload_to='documents/%Y/%m/%d/%H/%M/%S')
    signature_sample = models.FileField(upload_to='documents/%Y/%m/%d/%H/%M/%S')
    # note = models.CharField(max_length=200)
    # department = models.CharField(max_length=200)


class Contact(models.Model):
    title = models.CharField(max_length=200)
    name = models.CharField(max_length=200)
    mail = models.CharField(max_length=200)
    phone = models.CharField(max_length=200)
    # department = models.CharField(max_length=200)
    content = models.TextField()
    # capcha = models.CharField(max_length=200)


class Content(models.Model):
    name = models.CharField(max_length=200)
    content = models.CharField(max_length=20000)

    def __str__(self):
        return self.name
