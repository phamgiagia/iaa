from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('home/', views.index, name='index'),
    path('register/', views.register, name='register'),
    path('license/', views.license, name='license'),
    # path('procedure/', views.procedure, name='procedure'),
    path('pricing/', views.pricing, name='pricing'),
    path('contact/', views.contactForm, name='contact'),
    path('delivery/', views.delivery, name='delivery'),
    path('validate/', views.validate, name='validate'),
    path('guide/', views.guide, name='guide'),
    path('faq/', views.faq, name='faq'),
    path('sign/', views.sign, name='sign'),
    path('note/', views.note, name='note'),
    path('promotion/', views.promotion, name='promotion'),
    path('success/', views.success, name='success'),
    path('info/', views.info, name='info'),
    # path('vip/', views.vip, name='vip'),
    path('iaa/', views.iaa, name='iaa'),
    path('test/', views.test, name='test'),
]
