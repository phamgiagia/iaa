from django import forms

# class DocumentForm(forms.Form):
#     name = forms.CharField(max_length=200)
#     dob = forms.CharField(max_length=200)
#     national = forms.CharField(max_length=200)
#     address = forms.CharField(max_length=200)
#     ward = forms.CharField(max_length=200)
#     district = forms.CharField(max_length=200)
#     city = forms.CharField(max_length=200)
#     country = forms.CharField(max_length=200)
#     sex = forms.CharField(max_length=200)
#     phone = forms.CharField(max_length=200)
#     mail = forms.CharField(max_length=200)
#     eye_color = forms.CharField(max_length=200)
#     hair_color = forms.CharField(max_length=200)
#     height = forms.CharField(max_length=200)
#     license_time = forms.CharField(max_length=200)
#     urgent_license = forms.CharField(max_length=200)
#     license_front = forms.FileField()
#     license_back = forms.FileField()
#     pic = forms.FileField()
#     identity_card = forms.FileField()
#     signature_sample = forms.FileField()
#     note = forms.CharField(max_length=200)
#     department = forms.CharField(max_length=200)
from .models import Customer, Contact


class ContactForm(forms.ModelForm):
    class Meta:
        model = Contact
        fields = '__all__'

class DocumentForm(forms.ModelForm):
    class Meta:
        model = Customer
        fields = '__all__'
        # labels = {
        #     'name': _('Writer'),
        # }
        # help_texts = {
        #     'name': _('Some useful help text.'),
        # }
        # error_messages = {
        #     'name': {
        #         'max_length': _("This writer's name is too long."),
        #     },
        # }
