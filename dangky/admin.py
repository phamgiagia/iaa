from django.contrib import admin

# Register your models here.
from .models import Customer, Contact, Content
admin.site.register(Customer)
admin.site.register(Contact)
admin.site.register(Content)

# class CustomerAdmin(admin.ModelAdmin):
#     list_display = ('name','dob','file_link')
#     def file_link(self, obj):
#         if obj.license_front:
#             return "<a href='%s' >Download</a>" % (obj.license_front.url,)
#         else:
#             return "No attachment"
#     file_link.allow_tags = True
#     file_link.short_description = 'File Download'
#
# admin.site.register(Customer , CustomerAdmin)