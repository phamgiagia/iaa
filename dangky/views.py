from django.shortcuts import render

from django.template import RequestContext
from django.http import HttpResponseRedirect
from django.urls import reverse

from .models import Customer, Content
from .forms import DocumentForm, ContactForm


# Create your views here.
def index(request):
    return render(request, 'dangky/index.html', {})

def iaa(request):
    return render(request, 'dangky/ampiaa.html', {})


def license(request):
    return render(request, 'dangky/license.html', {})


def procedure(request):
    return render(request, 'dangky/procedure.html', {})


def pricing(request):
    return render(request, 'dangky/pricing.html', {})


def contact(request):
    return render(request, 'dangky/contact.html', {})


def delivery(request):
    return render(request, 'dangky/delivery.html', {})


def validate(request):
    return render(request, 'dangky/validate.html', {})


def guide(request):
    return render(request, 'dangky/guide.html', {})


def faq(request):
    return render(request, 'dangky/faq.html', {})


def sign(request):
    return render(request, 'dangky/sign.html', {})


def note(request):
    return render(request, 'dangky/note.html', {})


def promotion(request):
    return render(request, 'dangky/promotion.html', {})


def success(request):
    return render(request, 'dangky/success.html', {})


def info(request):
    return render(request, 'dangky/info.html', {})


def vip(request):
    return render(request, 'dangky/vip.html', {})


def register(request):
    # Handle file upload
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            # newdoc = Customer(name = request.POST['name'],
            #                   dob=request.POST['dob'],
            #                   national=request.POST['national'],
            #                   address=request.POST['address'],
            #                   ward=request.POST['ward'],
            #                   district=request.POST['district'],
            #                   city=request.POST['city'],
            #                   country=request.POST['country'],
            #                   sex=request.POST['sex'],
            #                   phone=request.POST['phone'],
            #                   mail=request.POST['name'],
            #                   eye_color=request.POST['eye_color'],
            #                   hair_color=request.POST['hair_color'],
            #                   height=request.POST['height'],
            #                   license_time=request.POST['name'],
            #                   urgent_license=request.POST['urgent_license'],
            #                   license_front=request.FILES['license_front'],
            #                   license_back=request.FILES['license_back'],
            #                   pic=request.FILES['pic'],
            #                   identity_card=request.FILES['identity_card'],
            #                   signature_sample=request.FILES['signature_sample'],
            #                   note=request.POST['note'],
            #                   department=request.POST['department'],
            #                   )
            # newdoc.save()

            # Redirect to the document list after POST
            return HttpResponseRedirect(reverse('success'))
    else:
        form = DocumentForm()  # A empty, unbound form

    # Load documents for the list page
    # documents = Customer.objects.all()


    # Render list page with the documents and the form
    return render(request, 'dangky/register.html', {'form': form})


def contactForm(request):
    # Handle file upload
    if request.method == 'POST':
        form = ContactForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            # Redirect to the document list after POST
            return HttpResponseRedirect(reverse('success'))
    else:
        form = ContactForm()  # A empty, unbound form

    # Load documents for the list page
    # documents = Customer.objects.all()


    # Render list page with the documents and the form
    return render(request, 'dangky/contact.html', {'form': form})


def test(request):
    # Handle file upload
    content = Content.objects.get(name="test")
    return render(request, 'dangky/test.html', {'content': content})
